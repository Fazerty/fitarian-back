import { createStore, combineReducers, applyMiddleware, Store } from "redux";

import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { systemReducer } from "./system/reducers";
import { messageReducer } from "./message/reducers";

const rootReducer = combineReducers({
  system: systemReducer,
  message: messageReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export function configureStore(): Store<AppState, any> {
  const middlewares = [thunkMiddleware];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  const store: Store<AppState, any> = createStore(
    rootReducer,
    composeWithDevTools(middleWareEnhancer)
  );

  return store;
}