
export type Language= 'en' | 'fr';
export interface SystemState {
  loggedIn: boolean;
  language: Language;
  userName: string;
}

// Describing the different ACTION NAMES available
export const UPDATE_LANGUAGE = "UPDATE_LANGUAGE";

interface UpdateLanguageAction {
  type: typeof UPDATE_LANGUAGE;
  payload: Language;
}

export type SystemActionTypes = UpdateLanguageAction;
