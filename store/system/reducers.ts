import { UPDATE_LANGUAGE, SystemState, SystemActionTypes } from "./types";

const initialState: SystemState = {
  loggedIn: false,
  userName: "",
  language: "en",
};

export function systemReducer(
  state = initialState,
  action: SystemActionTypes
): SystemState {

  switch (action.type) {
    case UPDATE_LANGUAGE: {
      return {
        ...state,
        language: action.payload
      };
    }
    default:
      return state;
  }
}
