import { UPDATE_LANGUAGE, Language } from "./types";

export function updateLanguage(newLanguage: Language) {
  return {
    type: UPDATE_LANGUAGE,
    payload: newLanguage
  };
}
