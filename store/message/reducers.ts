import {
  MessageState,
  SEND_MESSAGE,
  DELETE_MESSAGE,
  MessageActionTypes
} from "./types";

const initialState: MessageState = {
  messages: []
};

export function messageReducer(
  state = initialState,
  action: MessageActionTypes
): MessageState {
  switch (action.type) {
    case SEND_MESSAGE:
      return {
        messages: [...state.messages, action.payload]
      };
    case DELETE_MESSAGE:
      return {
        messages: state.messages.filter(
          message => message.timestamp !== action.meta.timestamp
        )
      };
    default:
      return state;
  }
}
