const withSass = require('@zeit/next-sass');
const withImages = require('next-images');

module.exports = withImages(
  withSass({
    sassLoaderOptions: {},

    withImages: withImages(),
    webpack(config, options) {
      config.module.rules.push({
        test: /\.graphql$/,
        exclude: /node_modules/,
        use: [options.defaultLoaders.babel, { loader: 'graphql-let/loader' }],
      });

      config.module.rules.push({
        test: /\.graphqls$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      });

      /*
     config.externals = {
        'react': 'React',
        'react-dom': 'ReactDOM'
      }
      */

      return config;
    },
  }),
);
