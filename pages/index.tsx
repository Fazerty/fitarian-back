import withApollo from '../lib/with-apollo';
import * as React from 'react';
import { NextPage } from 'next';
import AlimSearch from '../components/alim/alimSearch.component';
import About from '../components/about.page';
import Contact from '../components/contact.page';
import { Layout } from '../components/common/layout.component';
import { withRedux } from '../lib/with-redux';
import { compose } from 'redux';
import { Router, Route, Switch } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import News from '../components/news.page';
import css from 'styled-jsx/css';

const history = createMemoryHistory();

const indexStyle = css.global`
html, body {
  background-color: grey;
  position:absolute;
  height: 100%;
  width: 100%;
  overflow: hidden;
}
`

const Error = () => <div>Error</div>;

export const Index: NextPage = () => {
  return (
    <Router history={history}>
      <style jsx={undefined}>{indexStyle}</style>
      <Layout>
        <Switch>
          <Route exact path="/" component={AlimSearch} />
          <Route path="/about" component={About} />
          <Route path="/news" component={News} />
          <Route path="/contact" component={Contact} />
          <Route component={Error} />
        </Switch>
      </Layout>
    </Router>
  );
};

const IndexWrapper = compose(withApollo, withRedux)(Index);
export default IndexWrapper;
