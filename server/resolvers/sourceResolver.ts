import { getCustomRepository } from "typeorm";
import { Resolver, Query, Arg, Int } from "type-graphql"

import { Source } from "../entities";
import { SourceRepository } from "../repository/sourceRepository";

@Resolver()
export class SourceResolver {

  private readonly sourceRepository: SourceRepository = getCustomRepository(SourceRepository)


  @Query(_returns => Source, { nullable: true })
  source(@Arg("id", _type => Int) sourceId: number
  ) {
    return this.sourceRepository.findOne(sourceId);
  }

  @Query(_returns => [Source])
  sources(): Promise<Source[]> {
    return this.sourceRepository.find();
  }

}
