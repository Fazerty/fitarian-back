import { getCustomRepository } from "typeorm";
import { Resolver, Query, Arg, Int } from "type-graphql"

import { AlimGrp } from "../entities";
import { AlimGrpRepository } from "../repository/alimGrpRepository";

@Resolver(AlimGrp)
export class AlimGrpResolver {

  private readonly alimGrpRepository: AlimGrpRepository = getCustomRepository(AlimGrpRepository)


  @Query(_returns => AlimGrp, { nullable: true })
  alimGrp(@Arg("id", _type => Int) alimGrpId: number) {
    return this.alimGrpRepository.findOne(alimGrpId);
  }

  @Query(_returns => [AlimGrp])
  alimGrps(): Promise<AlimGrp[]> {
    return this.alimGrpRepository.find();
  }

}
