import { getCustomRepository, SelectQueryBuilder } from "typeorm";
import {
  Resolver,
  Query, Arg, Mutation, Int, FieldResolver, Root
} from "type-graphql"
import {
  Alim,
  AlimGrp,
  Compo
} from "../entities";
import { AlimRepository } from "../repository/alimRepository";
import { AlimInput } from "./types/alimInput";
import { AlimGrpRepository } from "../repository/alimGrpRepository";
import { AlimResult } from "./types/alimResult";
import { CompoRepository } from "../repository/compoRepository";

@Resolver(Alim)
export class AlimResolver {

  private readonly alimRepository: AlimRepository = getCustomRepository(AlimRepository)
  private readonly alimGrpRepository: AlimGrpRepository = getCustomRepository(AlimGrpRepository)
  private readonly compoRepository: CompoRepository = getCustomRepository(CompoRepository)

  /**
   * Returns an alim with the given id.
   *
   * @param {number} id: the id of the alim to be found
   * @returns {(Promise<Alim | undefined>)}
   * @memberof AlimResolver
   */
  @Query(_returns => Alim, { nullable: true })
  async alim(@Arg("id", _type => Int, { nullable: false }) id: number): Promise<Alim | undefined> {
    return this.alimRepository.findOne(id // , { relations: ['alim_grp'] } // REplaced by a fieldResolver
    );
  }

  @FieldResolver(_returns => AlimGrp)
  async alim_grp(@Root() alim: Alim): Promise<AlimGrp | undefined> {
    return await this.alimGrpRepository.findOne({
      where: { alim_grp_code: alim.alim_grp_code }
    });
  }

  @FieldResolver(_returns => [Compo])
  async compos(@Root() alim: Alim): Promise<Compo[] | undefined> {
    return await this.compoRepository.find({
      where: { alim_code: alim.alim_code }
    });
  }

  /**
   * Returns a list of alims having their name containing the search string and the total number of results.
   * The search is case insensitive.
   * The search is splitted in elements.
   * Each element must be contained in the name.
   * Returns nothing if search string is undefined.
   * Limit the number of result with the limit arg (10 by default).
   * Skip the number of result pages with the skip arg (0 by default).
   * The lang arg allows to choose between the french and the english name.
   * The result are ordered by name.
   *
   * @param {string} search
   * @param {number} [limit=10]
   * @param {number} [skip=0]
   * @returns {Promise<[Alim[], number]>}
   * @memberof AlimResolver
   */
  @Query(_returns => AlimResult)
  async alims(
    @Arg("limit", _type => Int, { nullable: true }) limit: number = 10,
    @Arg("offset", _type => Int, { nullable: true }) offset: number = 0,
    @Arg("search", { nullable: true }) search?: string,
    @Arg("lang", { nullable: true }) lang: string = 'en'
  ): Promise<AlimResult> {
    const searchElemts: string[] = search ? search.toUpperCase().trim().split(' ') : [];

    const nameField: string = lang === 'en' ? 'UPPER(alim_nom_eng)' : 'UPPER(alim_nom_fr)';

    let query: SelectQueryBuilder<Alim> = this.alimRepository.createQueryBuilder('alim');

    let pos: number = 0;
    for (const searchElemt of searchElemts) {

      // Use parameters to avoid sql injection.

      // to have unique parameter names.
      const parameterName: string = 'name' + pos;
      const queryStr: string = nameField + ' LIKE :' + parameterName;
      const parameter: any = {}
      parameter[parameterName] = `%${searchElemt}%`;
      ++pos;
      query.andWhere(queryStr, parameter);
    }
    query.limit(limit).offset(offset).orderBy(lang === 'en' ? { alim_nom_eng: 'ASC' } : { alim_nom_fr: 'ASC' })
    return new AlimResult(await query.getManyAndCount());
  }

  @Mutation(_returns => Alim)
  addAlim(@Arg("alim") alimInput: AlimInput): Promise<Alim> {
    const alim = this.alimRepository.create({
      ...alimInput,
    });
    return this.alimRepository.save(alim);
  }

  @Mutation(_returns => Alim)
  async rate(@Arg("alimId") alimId: number, @Arg("grpId") grpId: number): Promise<Alim> {
    // find the alim
    const alim = await this.alimRepository.findOne(alimId);
    if (!alim) {
      throw new Error("Invalid alim ID");
    }
    const grpInput: AlimGrp | undefined = await this.alimGrpRepository.findOne(grpId);
    if (!alim) {
      throw new Error("Invalid alim ID");
    }
    // add the new alim rate
    alim.alim_grp = grpInput;

    // return updated alim
    return await this.alimRepository.save(alim);
  }
}

