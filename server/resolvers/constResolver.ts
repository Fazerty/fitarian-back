import { getCustomRepository } from "typeorm";
import { Resolver, Query, Arg, Int } from "type-graphql"

import { Const } from "../entities";
import { ConstRepository } from "../repository/constRepository";

@Resolver(Const)
export class ConstResolver {

  private readonly constRepository: ConstRepository = getCustomRepository(ConstRepository)


  @Query(_returns => Const, { nullable: true })
  cnst(@Arg("id", _type => Int) alimId: number) {
    return this.constRepository.findOne(alimId);
  }

  @Query(_returns => [Const])
  cnsts(): Promise<Const[]> {
    return this.constRepository.find();
  }

}
