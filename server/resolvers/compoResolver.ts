import { getCustomRepository } from "typeorm";
import { Resolver, Query, Arg, Int } from "type-graphql"

import { Compo } from "../entities";
import { CompoRepository } from "../repository/compoRepository";

@Resolver(Compo)
export class CompoResolver {

  private readonly compoRepository: CompoRepository = getCustomRepository(CompoRepository)


  @Query(_returns => Compo, { nullable: true })
  compo(@Arg("id", _type => Int) compoId: number) {
    return this.compoRepository.findOne(compoId);
  }

  @Query(_returns => [Compo])
  compos(): Promise<Compo[]> {
    return this.compoRepository.find();
  }

}
