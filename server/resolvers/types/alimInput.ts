import {
  InputType,
  Field
} from "type-graphql";
import {
  Alim,
  // AlimGrp,
  // Compo
} from "../../entities";

@InputType()
export class AlimInput implements Partial<Alim>
{

  /**
   * code of the food
   *
   * @type {number}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_code?: number;

  /**
   * name of the food in French
   *
   * @type {string}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_nom_fr?: string;

  /**
   * ??
   *
   * @type {string}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_nom_index_fr?: string;

  /**
   * name of the food in English
   *
   * @type {string}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_nom_eng?: string;

  /**
   * ??
   *
   * @type {string}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_nom_index_eng?: string;

  /**
   * code of the food group
   *
   * @type {number}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_grp_code?: number;

  /**
   * the food group (or subgroup, or sub subgroup)
   *  Rem: This classification of groups is a bit strange.
   *  Have to link the alim to a group having code
   *  - the alim_grp_code if it's a group (with alim_ssgrp_code)
   *  - the alim_ssgrp_code if it's a subgroup (with alim_ssssgrp_code)
   *  - the alim_ssssgrp_code if it's a sub subgroup
   *
   * @type {AlimGrp}
   * @memberof Alim

  @Field(_type => AlimGrp)
  public alim_grpo?: AlimGrp;
*/
  /**
   *  The composants of this aliment.
   *
   * @type {Alim}
   * @memberof Compo

  @Field(_type => [Compo])
  public compos?: Compo[];
 */

  /**
   * code of the food subgroup
   *
   * @type {number}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_ssgrp_code?: number;


  /**
   * code of the food sub-subgroup
   *
   * @type {number}
   * @memberof Alim
   */
  @Field({ nullable: true })
  public alim_ssssgrp_code?: number;
}