import { ObjectType, Field } from "type-graphql";
import { Alim } from "../../entities";

@ObjectType()
export class AlimResult {

    /**
     * the total number of results
     *
     * @type {number}
     * @memberof Alim
     */
    @Field({ nullable: false })
    public count: number;

    /**
     * the alims
     *
     * @type {number}
     * @memberof Alim
     */
    @Field(_type => [Alim], { nullable: false })
    public result: Partial<Alim>[];


  constructor(results: [Alim[], number]) {
    this.count = results[1];
    this.result = results[0];
  }

}