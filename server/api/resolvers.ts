import { Resolver, Query } from 'type-graphql';
import { User } from '../entities/user';

/**
 * For tests. Will be replaced by real resolvers.
 *
 * @export
 * @class UserResolver
 */
@Resolver()
export class UserResolver {

  @Query(_returns => User)
  viewer(_parent: any, _args: any, _context: any, _info: any): User {
    const user: User = new User();
    user.id = 1;
    user.name = 'John Smith'
    user.status = 'cached'
    return user
  }
}

export default { Query }
