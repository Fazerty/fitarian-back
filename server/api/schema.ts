import { buildSchema } from "type-graphql";
import { UserResolver } from "./resolvers";
import {
  AlimGrpResolver, AlimResolver, CompoResolver, ConstResolver,
  SourceResolver
} from "../resolvers";

const schema = buildSchema({
  resolvers: [UserResolver, AlimGrpResolver, AlimResolver, CompoResolver, ConstResolver,
    SourceResolver
  ],
});

export default schema
