import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Const } from "./const";
import Alim from "./alim";
import { Source } from "./source";
import {
    ObjectType,
    Field, Int
} from "type-graphql"

/**
 * The food composition data of 2017 version of ANSES-CIQUAL table is available in the file
compo_2017 11 21.xml. Whenever possible, a value is given for the pair [food, component] (the food and
the component are described in the files alim_2017 11 21.xml and const_2017 11 21.xml).
 *
 * @export
 * @class Compo
 */
@Entity()
@ObjectType()
export class Compo {

    @Field(_type => Int)
    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     * code of the food
     *
     * @type {number}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_code?: number;


    /**
     *  The alim in this compo.
     *
     * @type {Alim}
     * @memberof Compo
     */
    @Field(_type => Alim)
    @ManyToOne(_type => Alim, alim => alim.compos)
    public alim?: Alim;


    /**
     * code of the component
     *
     * @type {number}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public const_code?: number;


    @Field(_type => Const)
    @ManyToOne(_type => Const, cnst => cnst.compos, {eager: true})
    public const?: Const;

    /**
     * it can be a value, a max value (example : "<10"), the indication
     * "trace" or a hyphen if the value is missing
     *
     * @type {string}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public teneur?: string;

    /**
     * min minimum value observed in the data-sources
     *
     * @type {string}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    min?: string;

    /**
     * max maximum value observed in the data-sources
     *
     * @type {string}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    max?: string;

    /**
     * code_confiance confidence code, which characterizes the quality of the average
     * content_value(A = very reliable to D = less reliable)
     *
     * @type {string}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    code_confiance?: string;


    /**
     *
    source_code code of the data-sources
     *
     * @type {number}
     * @memberof Compo
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    source_code?: number;

    /**
     * The source
     *
     * @type {Source}
     * @memberof Compo
     */
    @Field(_type => Source, { nullable: true })
    @ManyToOne(_type => Source, source => source.compos, {eager: true})
    public source?: Source;
}
