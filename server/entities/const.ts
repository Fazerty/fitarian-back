import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Compo } from "./compo";
import {
    ObjectType,
    Field, Int
} from "type-graphql"

/**
 * The components of the version 2017 of the ANSES-CIQUAL food composition table are listed in the table
const_2017 11 21.xml. A component has a name in French and in English.
 *
 * @export
 * @class Const
 */
@Entity()
@ObjectType()
export class Const {

    @Field(_type => Int)
    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     * code of the component
     *
     * @type {number}
     * @memberof Const
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public const_code?: number;

    /**
     *  The compo using this const.
     *
     * @type {Compo[]}
     * @memberof Const
     */

    @Field(_type => [Compo])
    @OneToMany(_type => Compo, compo => compo.const)
    public compos?: Compo[];

    /**
     * name of the component in French (includes unit)
     *
     * @type {string}
     * @memberof Const
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public const_nom_fr?: string;

    /**
     * name of the component in English (includes unit)
     *
     * @type {string}
     * @memberof Const
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public const_nom_eng?: string;
}
