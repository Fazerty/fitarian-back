import {
    Entity, PrimaryGeneratedColumn, Column, OneToMany
} from "typeorm";
import { Compo } from "./compo";
import { ObjectType, Field, Int } from "type-graphql"

/**
 * The data-sources which were used to produce published data of the version 2017 of the ANSES-CIQUAL
table are detailed in the file sources_2017 11 21.xml.
 *
 * @export
 * @class Source
 */
@Entity()
@ObjectType()
export class Source {

    @Field(_type => Int)
    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     * code of data-sources
     *
     * @type {number}
     * @memberof Source
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public source_code?: number;

    /**
     * name of data-sources
     *
     * @type {string}
     * @memberof Source
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public ref_citation?: string;

    /**
     *  The compo using this source.
     *
     * @type {Compo[]}
     * @memberof Source
     */
    @Field(_type => [Compo])
    @OneToMany(_type => Compo, compo => compo.source)
    public compos?: Compo[];
}
