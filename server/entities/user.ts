import { ObjectType, Field, Int } from "type-graphql"

/**
 * For tests
 *
 * @export
 * @class User
 */
@ObjectType()
export class User {

  @Field(_type => Int)
  id!: number;

  @Field()
  name!: string

  @Field()
  status!: string
}
