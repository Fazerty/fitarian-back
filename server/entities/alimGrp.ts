import {
    Entity, PrimaryGeneratedColumn, Column
    , OneToMany
} from "typeorm";
import Alim from "./alim";
import {
    ObjectType,
    Field, Int
} from "type-graphql"

/**
 * Foods have been arranged by ANSES-CIQUAL in groups, subgroups and sub-subgroups with common
 * characteristics, which can be the source of the food, the consumption habits, the type of consumers... This
 * classification is a choice of ANSES-CIQUAL but other types of classification exist.
 * The food groups, subgroups and sub-subgroups used in the 2017 version of ANSES-CIQUAL food
 * composition table are listed in the file alim_grp_2017 11 21.xml.
 *
 *
 * @export
 * @class AlimGrp
 */
@Entity()
@ObjectType()
export class AlimGrp {

    @Field(_type => Int)
    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     *  The alims in this group.
     *
     * @type {Alim[]}
     * @memberof AlimGrp
     */
    @Field(_type => [Alim])
    @OneToMany(_type => Alim, alim => alim.alim_grp)
    public alims?: Alim[];

    /**
     * code of the food group or subgroup or subsubgroup.
     * Rem: This classification is a bit strange.
     *  Have to create a code that will be
     *  - the group code if it's a group (with no subgroup code)
     *  - the subgroup code if it's a subgroup (with no sub subgroup code)
     *  - the sub subgroup code if it's a sub subgroup
     *
     * @type {number}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public grp_code?: number;

    /**
     * code of the food group
     *
     * @type {number}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_grp_code?: number;

    /**
     * name of the food group in French
     *
     * @type {string}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_grp_nom_fr?: string;

    /**
     * name of the food group in English
     *
     * @type {string}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_grp_nom_eng?: string;

    /**
     * code of the food subgroup
     *
     * @type {number}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_ssgrp_code?: number;

    /**
     * name of the food subgroup in French
     *
     * @type {string}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_ssgrp_nom_fr?: string;

    /**
     * name of the food subgroup in English
     *
     * @type {string}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_ssgrp_nom_eng?: string;

    /**
     * code of the food sub-subgroup
     *
     * @type {number}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_ssssgrp_code?: number;

    /**
     * name of the food sub-subgroup in French
     *
     * @type {string}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_ssssgrp_nom_fr?: string;

    /**
     * name of the food sub-subgroup in English
     *
     * @type {string}
     * @memberof AlimGrp
     */
    @Field({ nullable: true })
    @Column({ nullable: true })
    public alim_ssssgrp_nom_eng?: string;

}
