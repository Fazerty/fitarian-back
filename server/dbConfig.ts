import 'reflect-metadata';
import { Connection, ConnectionOptions, createConnection } from 'typeorm';
import { LoggerOptions } from 'typeorm/logger/LoggerOptions';
import {
  PostRefactoring1584104793844, PostRefactoring1584451152554, PostRefactoring1584451512647
} from '../database/migration/pg';
import {
   AlimGrp, Compo, Const, Source
} from './entities';
import Alim from './entities/alim';

const isDevelopment = process.env.NODE_ENV !== 'production';

/**
 * The default database connection parameters.
 *
 */
const databaseOptions: ConnectionOptions = {
  type: 'postgres',
  database: 'fitarian',
  host: 'localhost',
  port: 5432,
  username: 'fitarian',
  password: 'fitarian',

  synchronize: false,
  migrationsRun: true,
  logging: (isDevelopment ? 'query' : 'error') as LoggerOptions,

  logger: isDevelopment ? 'simple-console' : 'debug',
  // as 'advanced-console' | 'simple-console' | 'file' | 'debug' | Logger,
  entities: [Alim, AlimGrp, Compo, Const, Source
  ],
  migrationsTableName: 'custom_migration',
  migrations: [PostRefactoring1584104793844, PostRefactoring1584451152554, PostRefactoring1584451512647],
};

/**
 *
 *
 * @export
 * @returns {Promise<Connection>}
 */
export async function createWebDbConnection(): Promise<Connection> {
  const connection: Connection = await createConnection(databaseOptions);
  if (isDevelopment) {
  }
  return connection;
}
