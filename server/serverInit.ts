import 'reflect-metadata';
import { IncomingMessage, ServerResponse } from 'http'
import express from 'express'
import next from 'next'
import { createWebDbConnection } from './dbConfig'
import { ImportXmlService } from '../utils/importXmlService';
import Server from 'next/dist/next-server/server/next-server';
import { ApolloServer } from 'apollo-server-express';
import schema from './api/schema'
import cors, { CorsOptions } from 'cors';

/**
 * the port of the express server
 */
const port = parseInt(process.env.PORT || '3000', 10)

/**
 * Is it the dev env?
 */
const dev = process.env.NODE_ENV !== 'production'

/**
 * the path of the apollo/graphql server
 */
export const graphqlPath: string = '/api/graphql';

// @ts-ignore
const debugPlugin = {

  // Fires whenever a GraphQL request is received from a client.
  requestDidStart(requestContext: any) {
    console.log('Request started! Query:\n' +
      requestContext.request.query);

    return {

      // Fires whenever Apollo Server will parse a GraphQL
      // request to create its associated document AST.
      parsingDidStart(_requestContext: any) {
        console.log('Parsing started!');
      },

      // Fires whenever Apollo Server will validate a
      // request's document AST against your GraphQL schema.
      validationDidStart(_requestContext: any) {
        console.log('Validation started!');
      },

      didEncounterErrors(
        requestContext: any,
      ) {
        console.log('errrrooooor!');
        console.log(requestContext);
      }
    }
  },
};

/**
 * Init the server and start it.
 * Will
 *  - create the db connection
 *  - import the data from xml if needed.
 *  - create an express server
 *  - create a next server
 *  - create an appolo server
 *
 *
 * @export
 */
export async function serverInit() {

  const app: Server = next({ dev })

  const handle = app.getRequestHandler()

  await app.prepare();

  await createWebDbConnection();
  await ImportXmlService.importAll();

  const server = express();
  const corsOptions: CorsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 200
  }
  server.use(cors(corsOptions))

  const appoloServer = new ApolloServer({
    schema: await schema,
    plugins: [
      // debugPlugin
    ]
  });

  appoloServer.applyMiddleware({ app: server, path: graphqlPath });


  server.all('*', (req: IncomingMessage, res: ServerResponse) => {
    return handle(req, res)
  })

  server.listen(port, err => {
    if (err) { throw err }
    // tslint:disable-next-line:no-console
    console.log(
      `> Server listening at http://localhost:${port} as ${
      dev ? 'development' : process.env.NODE_ENV
      }`
    )
  })

}