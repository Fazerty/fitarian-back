import { AlimGrp } from "../entities";
import { AlimGrpRepository } from "../repository/alimGrpRepository";
import { getCustomRepository } from "typeorm";


export class AlimGrpService {


    alimGrpRepository: AlimGrpRepository = getCustomRepository(AlimGrpRepository);
    /**
     * Creates an instance of AlimGrp from xml2js data.
     *
     * @param {{
     *         alim_grp_code: string,
     *         alim_grp_nom_fr: string,
     *         alim_grp_nom_eng: string,
     *         alim_ssgrp_code: string,
     *         alim_ssgrp_nom_fr: string,
     *         alim_ssgrp_nom_eng: string,
     *         alim_ssssgrp_code: string,
     *         alim_ssssgrp_nom_fr: string,
     *         alim_ssssgrp_nom_eng: string
     *     }} element
     * @memberof AlimGrp
     */
    async create(element: {
        alim_grp_code: string,
        alim_grp_nom_fr: string,
        alim_grp_nom_eng: string,
        alim_ssgrp_code: string,
        alim_ssgrp_nom_fr: string,
        alim_ssgrp_nom_eng: string,
        alim_ssssgrp_code: string,
        alim_ssssgrp_nom_fr: string,
        alim_ssssgrp_nom_eng: string
    }): Promise<AlimGrp | undefined> {
        if (element) {
            const alimGrp: AlimGrp = new AlimGrp();
            alimGrp.alim_grp_code = parseInt(element.alim_grp_code, 10);
            alimGrp.alim_grp_nom_fr = element.alim_grp_nom_fr;
            alimGrp.alim_grp_nom_eng = element.alim_grp_nom_eng;

            alimGrp.alim_ssgrp_code = parseInt(element.alim_ssgrp_code, 10) !== 0 ? parseInt(element.alim_ssgrp_code, 10) : undefined;
            alimGrp.alim_ssssgrp_code = parseInt(element.alim_ssssgrp_code, 10) !== 0 ? parseInt(element.alim_ssssgrp_code, 10) : undefined;

            alimGrp.grp_code = alimGrp.alim_ssssgrp_code || alimGrp.alim_ssgrp_code || alimGrp.alim_grp_code;

            if (alimGrp.alim_ssgrp_code) {
                alimGrp.alim_ssgrp_nom_fr = element.alim_ssgrp_nom_fr;
                alimGrp.alim_ssgrp_nom_eng = element.alim_ssgrp_nom_eng;
            }
            if (alimGrp.alim_ssssgrp_code) {
                alimGrp.alim_ssssgrp_nom_fr = element.alim_ssssgrp_nom_fr;
                alimGrp.alim_ssssgrp_nom_eng = element.alim_ssssgrp_nom_eng;
            }
            return this.alimGrpRepository.save(alimGrp);
        }
    }

    /**
     * Create missing grp. Some are needed by alim
     *  rem : A lot of redundancy in the xml and missing groups and subgroups (401, 403, 503, 8 are needed in alim).
     *   Table were created from the xml structure. It could be changed later to avoid redundancy.
     *
     * @memberof AlimGrpService
     */
    async createMissingGrps() {
        const missingSsGrps: any[] = await this.alimGrpRepository.query('select distinct alim_grp_code, alim_grp_nom_fr, alim_grp_nom_eng, alim_ssgrp_code, alim_ssgrp_nom_fr, alim_ssgrp_nom_eng from public.alim_grp ' +
            'where alim_ssgrp_code in (select alim_ssgrp_code from public.alim_grp except select alim_ssgrp_code from public.alim_grp where alim_ssssgrp_code IS NULL)')
        const missingGrps: any[] = await this.alimGrpRepository.query('select distinct alim_grp_code, alim_grp_nom_fr, alim_grp_nom_eng from public.alim_grp ' +
            'where alim_grp_code in (select alim_grp_code from public.alim_grp  except select alim_grp_code from public.alim_grp where alim_ssgrp_code IS NULL)')
        for (const element of missingSsGrps as {
            alim_grp_code: number,
            alim_grp_nom_fr: string,
            alim_grp_nom_eng: string,
            alim_ssgrp_code: number,
            alim_ssgrp_nom_fr: string,
            alim_ssgrp_nom_eng: string,
        }[]) {
            const alimGrp: AlimGrp = new AlimGrp();
            alimGrp.alim_grp_code = element.alim_grp_code;
            alimGrp.alim_grp_nom_fr = element.alim_grp_nom_fr;
            alimGrp.alim_grp_nom_eng = element.alim_grp_nom_eng;

            alimGrp.alim_ssgrp_code = element.alim_ssgrp_code;

            alimGrp.grp_code = alimGrp.alim_ssssgrp_code || alimGrp.alim_ssgrp_code || alimGrp.alim_grp_code;

            if (alimGrp.alim_ssgrp_code) {
                alimGrp.alim_ssgrp_nom_fr = element.alim_ssgrp_nom_fr;
                alimGrp.alim_ssgrp_nom_eng = element.alim_ssgrp_nom_eng;
            }
            await this.alimGrpRepository.save(alimGrp);

        }
        for (const element of missingGrps as {
            alim_grp_code: number,
            alim_grp_nom_fr: string,
            alim_grp_nom_eng: string,
        }[]) {
            const alimGrp: AlimGrp = new AlimGrp();
            alimGrp.alim_grp_code = element.alim_grp_code;
            alimGrp.alim_grp_nom_fr = element.alim_grp_nom_fr;
            alimGrp.alim_grp_nom_eng = element.alim_grp_nom_eng;

            alimGrp.grp_code = alimGrp.alim_ssssgrp_code || alimGrp.alim_ssgrp_code || alimGrp.alim_grp_code;

            await this.alimGrpRepository.save(alimGrp);

        }
    }
}
