import { Alim, AlimGrp } from "../entities";
import { AlimRepository } from "../repository/alimRepository";
import { getCustomRepository } from "typeorm";

export class AlimService {

    private alimRepository: AlimRepository = getCustomRepository(AlimRepository);

    /**
     * Creates an instance of Alim from xml2js data.
     * It will creates relationships with alimGrp entities.
     *
     * @param {{
     *         alim_code: string,
     *         alim_nom_fr: string,
     *         alim_nom_index_fr: string,
     *         alim_nom_eng: string,
     *         alim_nom_index_eng: string,
     *         alim_grp_code: string,
     *         alim_ssgrp_code: string,
     *         alim_ssssgrp_code: string
     *     }} element
     * @memberof Alim
     */
    async create(element: {
        alim_code: string,
        alim_nom_fr: string,
        alim_nom_index_fr: string,
        alim_nom_eng: string,
        alim_nom_index_eng: string,
        alim_grp_code: string,
        alim_ssgrp_code: string,
        alim_ssssgrp_code: string
    }, grpCodeId: Map<number, number>
    ): Promise<Alim | undefined> {
        if (element) {
            const alim: Alim = new Alim();
            alim.alim_code = parseInt(element.alim_code, 10);
            alim.alim_nom_fr = element.alim_nom_fr;
            alim.alim_nom_index_fr = element.alim_nom_index_fr;
            alim.alim_nom_eng = element.alim_nom_eng;
            alim.alim_nom_index_eng = element.alim_nom_index_eng;

            alim.alim_grp_code = parseInt(element.alim_grp_code, 10);
            alim.alim_ssgrp_code = parseInt(element.alim_ssgrp_code, 10) !== 0 ? parseInt(element.alim_ssgrp_code, 10) : undefined;
            alim.alim_ssssgrp_code = parseInt(element.alim_ssssgrp_code, 10) !== 0 ? parseInt(element.alim_ssssgrp_code, 10) : undefined;

            const grp_code: number | undefined = alim.alim_ssssgrp_code || alim.alim_ssgrp_code || alim.alim_grp_code;
            if (grp_code) {
                const grp: AlimGrp = new AlimGrp();
                grp.id = grpCodeId.get(grp_code);
                if (grp.id) {
                    // alim.alim_grpo = grp;
                }
            }

            return this.alimRepository.save(alim);
        }

    }

}
