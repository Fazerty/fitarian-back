import { SourceRepository } from "../repository/sourceRepository";
import { getCustomRepository } from "typeorm";
import { Source } from "../entities";

export class SourceService {

    sourceRepository: SourceRepository = getCustomRepository(SourceRepository);

    /**
     * Creates an instance of Source from xml2js data.
     *
     * @param {{
     *         source_code: string,
     *         ref_citation: string
     *     }} element
     * @memberof Source
     */
    async create(element: {
        source_code: string,
        ref_citation: string
    }) {
        if (element) {
            const source: Source = new Source();
            source.source_code = parseInt(element.source_code);
            source.ref_citation = element.ref_citation;

            return this.sourceRepository.save(source);
        }
      }
}
