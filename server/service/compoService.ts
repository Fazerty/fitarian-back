import {
    Compo,
    // Alim,
    Const, Source, Alim
} from "../entities";
import { CompoRepository } from "../repository/compoRepository";
import { getCustomRepository } from "typeorm";

export class CompoService {


    private compoRepository: CompoRepository = getCustomRepository(CompoRepository);

    /**
     * Creates an instance of Compo from xml2js data.
     * It will creates relationships with alim, const and source entities.
     *
     * @param {({
     *         alim_code: string,
     *         const_code: string,
     *         teneur: string,
     *         min: string | { '$': { missing: string } },
     *         max: string | { '$': { missing: string } },
     *         code_confiance: string | { '$': { missing: string } },
     *         source_code: string | { '$': { missing: string } }
     *     })} element
     * @memberof Compo
     */
    async create(element: {
        alim_code: string,
        const_code: string,
        teneur: string,
        min: string | { '$': { missing: string } },
        max: string | { '$': { missing: string } },
        code_confiance: string | { '$': { missing: string } },
        source_code: string | { '$': { missing: string } }
    }, alimCodeId: Map<number, number>, cnstCodeId: Map<number, number>, sourceCodeId: Map<number, number>) {
        if (element) {
            const compo: Compo = new Compo();
            compo.alim_code = parseInt(element.alim_code, 10) !== 0 ? parseInt(element.alim_code, 10) : undefined;
            compo.const_code = parseInt(element.const_code, 10) !== 0 ? parseInt(element.const_code, 10) : undefined;
            compo.teneur = element.teneur !== '-' ? element.teneur : undefined;

            if (typeof (element.min) === 'string') {
                compo.min = element.min;
            }

            if (typeof (element.max) === 'string') {
                compo.max = element.max;
            }

            if (typeof (element.code_confiance) === 'string') {
                compo.code_confiance = element.code_confiance;
            }

            if (typeof (element.source_code) === 'string') {
                compo.source_code = parseInt(element.source_code);
            }

            if (compo.alim_code) {
                const alim: Alim = new Alim();
                alim.id= alimCodeId.get(compo.alim_code);
                if (alim.id) {
                   // compo.alim = alim;
                }
            }

            if (compo.const_code) {
                const cnst: Const = new Const();
                cnst.id = cnstCodeId.get(compo.const_code);
                if (cnst.id) {
                    compo.const = cnst;
                }

            }

            if (compo.source_code) {
                const source: Source = new Source();
                source.id = sourceCodeId.get(compo.source_code);
                if (source) {
                    compo.source = source;
                }

            }

            return this.compoRepository.save(compo);
        }
    }
}
