import { ConstRepository } from "../repository/constRepository";
import { getCustomRepository } from "typeorm";
import { Const } from "../entities";


export class ConstService {

    constRepository: ConstRepository = getCustomRepository(ConstRepository);

    /**
     * Creates an instance of Const from xml2js data.
     * @param {{
     *         const_code: string,
     *         const_nom_fr: string,
     *         const_nom_eng: string
     *     }} element
     * @memberof Const
     */
    async create(element: {
        const_code: string,
        const_nom_fr: string,
        const_nom_eng: string
    }) {
        if (element) {
            const cnst: Const = new Const();
            cnst.const_code = parseInt(element.const_code);
            cnst.const_nom_fr = element.const_nom_fr;
            cnst.const_nom_eng = element.const_nom_eng;

            return this.constRepository.save(cnst);
        }
    }
}
