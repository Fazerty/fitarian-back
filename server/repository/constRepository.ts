import { EntityRepository, Repository } from 'typeorm';
import {Const} from '../entities';

/**
 * repository for the Const entity.
 */
@EntityRepository(Const)
export class ConstRepository extends Repository<Const> {

}