import { EntityRepository, Repository } from 'typeorm';
import {AlimGrp} from '../entities';

/**
 * repository for the AlimGrp entity.
 */
@EntityRepository(AlimGrp)
export class AlimGrpRepository extends Repository<AlimGrp> {

}