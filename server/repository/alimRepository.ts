import { EntityRepository, Repository } from 'typeorm';
import { Alim } from '../entities/alim';

/**
 * repository for the Alim entity.
 */
@EntityRepository(Alim)
export class AlimRepository extends Repository<Alim> {

}