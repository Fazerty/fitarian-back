import { EntityRepository, Repository } from 'typeorm';
import {Source} from '../entities';

/**
 * repository for the Source entity.
 */
@EntityRepository(Source)
export class SourceRepository extends Repository<Source> {

}