import 'jsdom-global/register';
import { ImportXml } from '../../utils/importXml';
import { createWebDbConnection } from '../../server/dbConfig';
import { AlimRepository } from '../../server/repository/alimRepository';
import { getCustomRepository } from 'typeorm';
import { AlimGrpRepository } from '../../server/repository/alimGrpRepository';
import { ConstRepository } from '../../server/repository/constRepository';
import { SourceRepository } from '../../server/repository/sourceRepository';
import { CompoRepository } from '../../server/repository/compoRepository';
import { ImportXmlService } from '../../utils/importXmlService';

describe.skip('Xml', () => {
  describe('import xml', () => {
    it('imported table alim_2017 11 21 has more than 1 entry ', async function() {
      const result: any = await ImportXml.importXml(
        '../xml/alim_2017 11 21.xml',
      );
      expect(result.TABLE.ALIM.length).toBeGreaterThan(1);
    });

    it('imported table alim_grp_2017 11 21 has more than 1 entry ', async function() {
      const result: any = await ImportXml.importXml(
        '../xml/alim_grp_2017 11 21.xml',
      );
      expect(result.TABLE.ALIM_GRP.length).toBeGreaterThan(1);
    });

    it('imported table compo_2017 11 21 has more than 1 entry ', async function() {
      const result: any = await ImportXml.importXml(
        '../xml/compo_2017 11 21.xml',
      );
      expect(result.TABLE.COMPO.length).toBeGreaterThan(1);
    });

    it('imported table const_2017 11 21 has more than 1 entry ', async function() {
      const result: any = await ImportXml.importXml(
        '../xml/const_2017 11 21.xml',
      );
      expect(result.TABLE.CONST.length).toBeGreaterThan(1);
    });

    it('imported table sources_2017 11 21 has more than 1 entry ', async function() {
      const result: any = await ImportXml.importXml(
        '../xml/sources_2017 11 21.xml',
      );
      expect(result.TABLE.SOURCES.length).toBeGreaterThan(1);
    });
  });

  describe('import in db', () => {
    let alimRepository: AlimRepository;
    let alimGrpRepository: AlimGrpRepository;
    let constRepository: ConstRepository;
    let sourceRepository: SourceRepository;
    let compoRepository: CompoRepository;

    beforeAll(async () => {
      jest.setTimeout(6000000);
      await createWebDbConnection();
      await ImportXmlService.importAll();
      alimRepository = getCustomRepository(AlimRepository);
      alimGrpRepository = getCustomRepository(AlimGrpRepository);
      constRepository = getCustomRepository(ConstRepository);
      sourceRepository = getCustomRepository(SourceRepository);
      compoRepository = getCustomRepository(CompoRepository);
    });

    it('alim db is not empty', async function() {
      expect(await alimRepository.count()).toBeGreaterThan(1);
    });

    it('alimGrp db is not empty', async function() {
      expect(await alimGrpRepository.count()).toBeGreaterThan(1);
    });

    it('const is not empty', async function() {
      expect(await constRepository.count()).toBeGreaterThan(1);
    });

    it('source is not empty', async function() {
      expect(await sourceRepository.count()).toBeGreaterThan(1);
    });

    it('compo is not empty', async function() {
      expect(await compoRepository.count()).toBeGreaterThan(1);
    });

    it('alim db is not empty', async function() {
      expect(await alimRepository.count()).toBeGreaterThan(1);
    });

    it('alimGrp has no missing grp', async function() {
      const missingGrps: any[] = await alimGrpRepository.query(
        'select distinct alim_grp_code, alim_grp_nom_fr, alim_grp_nom_eng from public.alim_grp ' +
          'where alim_grp_code in (select alim_grp_code from public.alim_grp  except select alim_grp_code from public.alim_grp where alim_ssgrp_code IS NULL)',
      );
      expect(missingGrps.length).toBe(0);
    });

    it('alimGrp has no missing grp', async function() {
      const missingSsGrps: any[] = await alimGrpRepository.query(
        'select distinct alim_grp_code, alim_grp_nom_fr, alim_grp_nom_eng, alim_ssgrp_code, alim_ssgrp_nom_fr, alim_ssgrp_nom_eng from public.alim_grp ' +
          'where alim_ssgrp_code in (select alim_ssgrp_code from public.alim_grp except select alim_ssgrp_code from public.alim_grp where alim_ssssgrp_code IS NULL)',
      );
      expect(missingSsGrps.length).toBe(0);
    });

    it('all alim has a grp', async function() {
      expect(
        await alimRepository.count({ where: { alim_grp: undefined } }),
      ).toBe(0);
    });

    it('all compo has an alim', async function() {
      expect(await compoRepository.count({ where: { alim: undefined } })).toBe(
        0,
      );
    });

    it('all compo has an const', async function() {
      expect(await compoRepository.count({ where: { const: undefined } })).toBe(
        0,
      );
    });
  });
});
