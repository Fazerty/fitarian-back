import 'reflect-metadata';
import 'jsdom-global/register';
import React, { Component } from 'react';
import { mount, ReactWrapper } from 'enzyme';
import ApolloClient from 'apollo-client';
import { ApolloProvider, useQuery } from '@apollo/react-hooks';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { act } from 'react-dom/test-utils';
import serverSchema from '../../server/api/schema';
import { SchemaLink } from 'apollo-link-schema';
import { addMockFunctionsToSchema, IMocks } from 'graphql-tools';
import { gql } from 'apollo-server-express';
import { Compo } from '../../server/entities';

describe('Compo schema', () => {
  describe('Provider tests', () => {
    let client: ApolloClient<NormalizedCacheObject>;

    beforeAll(async () => {
      const schema = await serverSchema;

      const mocks: IMocks = {
        // Query: () => ,
        // Mutation: () => ...
      };

      addMockFunctionsToSchema({
        schema,
        mocks,
        preserveResolvers: false,
      });

      client = new ApolloClient({
        link: new SchemaLink({ schema }),
        cache: new InMemoryCache(),
      });
    });

    it('should render without throwing an error', async function() {
      let wrap!: ReactWrapper<any, Readonly<{}>, Component<{}, {}, any>>;

      const QUERY = gql`
        query Test {
          compo(id: 1045397) {
            id
            const_code
          }
        }
      `;

      function TestComponent() {
        const { data } = useQuery(QUERY);
        if (data) {
          const { compo }: { compo: Partial<Compo> } = data;
          if (compo) {
            return (
              <p>
                {JSON.stringify(compo)}
              </p>
            );
          } else {
            return <p>no source</p>;
          }
        }
        return <p>no data</p>;
      }

      await act(async () => {
        wrap = mount(
          <ApolloProvider client={client}>
            <TestComponent />
          </ApolloProvider>,
        );
      });

      expect(wrap.find('p').text()).toContain('id');
    });
  });
});
