import 'reflect-metadata';
import 'jsdom-global/register';
import React, { Component } from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { Index } from '../../pages';
import ApolloClient from 'apollo-client';
import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { act } from 'react-dom/test-utils';
import serverSchema from '../../server/api/schema';
import { SchemaLink } from 'apollo-link-schema';
import { addMockFunctionsToSchema, IMocks } from 'graphql-tools';
import configureStore, {
  MockStoreCreator,
  MockStoreEnhanced,
} from 'redux-mock-store';
import { AppState } from '../../store/store';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { MessageState } from '../../store/message/types';
import { SystemState } from '../../store/system/types';

const middlewares: any[] = [thunk];
const mockStore: MockStoreCreator<AppState, any> = configureStore(middlewares);

jest.mock('styled-jsx/css');

describe('Pages', () => {
  describe('Index', () => {
    let client: ApolloClient<NormalizedCacheObject>;
    let store: MockStoreEnhanced<AppState, any>;

    beforeAll(async () => {
      const schema = await serverSchema;

      const state: AppState = {
        system: {
          loggedIn: false,
          language: 'en',
          userName: '',
        } as SystemState,
        message: {} as MessageState,
      };

      store = mockStore(state);

      const mocks: IMocks = {
        // Query: () => ,
        // Mutation: () => ...
      };

      addMockFunctionsToSchema({
        schema,
        mocks,
        // preserveResolvers: true
      });

      client = new ApolloClient({
        link: new SchemaLink({ schema }),
        cache: new InMemoryCache(),
      });
    });

    it('should render without throwing an error', async function() {
      let wrap!: ReactWrapper<any, Readonly<{}>, Component<{}, {}, any>>;
      await act(async () => {
        wrap = mount(
          <Provider store={store}>
            <ApolloProvider client={client}>
              <Index />
            </ApolloProvider>
          </Provider>,
        );
      });
      const cherio: Cheerio = wrap.render();
      expect(cherio.find('.subtitle').text()).toContain("A food composition table");
    });
  });
});
