import React, { Component } from 'react';
import aboutStyle from './about.style';
import { SystemState } from '../store/system/types';
import { connect } from 'react-redux';
import { AppState } from '../store/store';
import QRCode from 'qrcode.react';

interface AboutProps {
  system: SystemState;
}

export class About extends Component<AboutProps> {
  render() {
    return (
      <div className="about">
        <style jsx={undefined}>{aboutStyle}</style>
        <div className="about__content">
          <div className="about__title">
            {this.props.system.language === 'en' ? 'About' : 'A propos'}
          </div>
          <hr />
          <div className="about__title about__title--small">Fitarian</div>
          {this.props.system.language === 'en' ? (
            <div>
              <div className="about__text">
                This server is used by Fitarian, a free and open source mobile
                app to calculate daily kcal intakes.
              </div>
              <div className="about__text">
                It's source code is available at{' '}
                <a href="https://bitbucket.org/Fazerty/fitarian">bitbucket</a>
              </div>
              <div className="about__text">
                <div className="rows">
                  <div>
                    <QRCode value="https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40fabien.dubail/fitarian-1103147d3e644c929edce3cdae02bac0-signed.apk"></QRCode>
                  </div>
                  <div>
                    The apk is available here:{' '}
                    <a href="https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40fabien.dubail/fitarian-1103147d3e644c929edce3cdae02bac0-signed.apk">
                      apk at expo
                    </a>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div>
              <div className="about__text">
                Ce serveur est utilisé par Fitarian, une application mobile
                gratuite et open source permettant de calculer les consommations
                journalières en kcal.
              </div>
              <div className="about__text">
                Le code source is disponible sur{' '}
                <a href="https://bitbucket.org/Fazerty/fitarian">bitbucket</a>
              </div>
              <div className="about__text">
                <div className="rows">
                  <div>
                    <QRCode value="https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40fabien.dubail/fitarian-1103147d3e644c929edce3cdae02bac0-signed.apk"></QRCode>
                  </div>
                  <div>
                    L'apk est disponible ici:{' '}
                    <a href="https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40fabien.dubail/fitarian-1103147d3e644c929edce3cdae02bac0-signed.apk">
                      apk chez expo
                    </a>
                  </div>
                </div>
              </div>
            </div>
          )}
          <hr />
          <div className="about__title about__title--small">Ciqual</div>

          {this.props.system.language === 'en' ? (
            <div>
              <div className="about__text">
                The present data and information are made available to the
                public by the French Agency for Food, Environmental and
                Occupational Health & Safety (ANSES).
              </div>
              <div className="about__text">
                They must not be reproduced in any form without clear indication
                of the source: "ANSES-CIQUAL French food composition table
                version 2017"
              </div>
              <div className="about__text">
                or, in more detail, for a scientific publication: " French
                Agency for Food, Environmental and Occupational Health & Safety.
                ANSES-CIQUAL French food composition table version 2017.
                Retrieved DD/MM/YYYY from the Ciqual homepage
                https://ciqual.anses.fr/ "
              </div>
            </div>
          ) : (
            <div>
              <div className="about__text">
                La réutilisation des informations mises en ligne sur le site
                Anses-Ciqual est soumise à la condition que ces dernières ne
                soient pas altérées, que leur sens ne soit pas dénaturé et que
                la source ainsi que la version soient mentionnées, conformément
                aux dispositions de la loi n° 2016-1321 du 7 octobre 2016 pour
                une République numérique, et du code des relations entre le
                public et l'administration.
              </div>
              <div className="about__text">
                Les données de la table Ciqual sont ouvertes au public et
                téléchargeables gratuitement ci-dessous ou via le portail
                data.gouv.fr.
              </div>
              <div className="about__text">
                La réutilisation de ces données est possible dans les libertés
                et conditions prévues par la Licence Ouverte.
              </div>
              <div className="about__text">
                "Agence nationale de sécurité sanitaire de l’alimentation, de
                l’environnement et du travail. Table de composition
                nutritionnelle des aliments Ciqual 2017. Site internet Ciqual
                https://ciqual.anses.fr/ "
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const AboutRedux = connect(mapStateToProps, {})(About);

export default AboutRedux;
