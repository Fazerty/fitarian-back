import css from 'styled-jsx/css'

export default css`

.alim-search {
  display: flex;
  width: 50%;
  flex-direction : column;
  justify-content: center;
  align-items: center;
  align-content: center;
}
@media (max-width: 600px) {
  .alim-search {
    width: 90%;
  }
}

.alim-search__title{
  text-align: center;
  font-weight: bold;
  font-size: xx-large;
  color: white;
  padding: 2px;
}

.alim-search__subtitle{
  text-align: center;
  font-size: large;
  color: white;
  padding-bottom: 2px;
}

.alim-search__input {
  width: 90%;
}

.results-info {
  text-align: center;
  font-weight: bold;
  padding: 8px 16px;
  border: 1px solid #ddd;
  background-color: whitesmoke;

}

.results-info__page-number{
  color: gray;
}

.pagination {
  text-align: center;
  border: 1px solid #ddd;
  background-color: whitesmoke;
  display:flex;
  flex-direction: row;
  border-radius: 2em;
}
.pagination__button--rounded-left {
  border-top-left-radius: 2em;
  border-bottom-left-radius: 2em;
}

.pagination__button--rounded-right {
  border-top-right-radius: 2em;
  border-bottom-right-radius: 2em;
}

.pagination__button {
  color: rgb(56, 50, 50);
  padding: 8px 16px;
  font-weight: bold;
  font-size: 18px;
  text-decoration: none;
  border: 1px solid #ddd;
}

.pagination__button:disabled {
  background-color: white;
  color: rgb(202, 190, 190);
}

.alim-search__results{
  margin-top: 10px;
  height: 22em;
  display: block;
  width: 100%;
  overflow: auto;
}

.alim-search__result-list {
  padding: 6px;
  height: auto;
  list-style-type: none;
  background-color:white;
}

.alim-search__result-header{
  width: 10%;
  padding-top: 5px;
  padding-bottom: 5px;
  margin-bottom: 5px;
  padding-left: 10px;
  font-size: 17px;
  font-weight: bold;
  background-color: white;
  border-bottom: 2px solid blue;
}
.alim-search__result {
  padding-top: 5px;
  padding-bottom: 5px;
  padding-left: 10px;
  background-color: white;
}


.alim-search__result--light {
  background-color: rgb(230, 235, 230);
 }
 .alim-search__result:hover {
   color: blue;
 }

.alim-search__result-list li:last-child {
  border-bottom: none
}

.modal__content {
  padding: 1%;
  background-color: rgb(235, 226, 226);
}

.modal__close-button {
position: absolute;
right: 0;
top: 0;
padding: 8px 16px;
margin: 4px;
display: inline-block;
border-radius: 2px;
vertical-align: middle;
color: white;
font-weight: bold;
font-size: 18px;
background-color: rgb(22, 49, 202);
text-align: center;
}
`