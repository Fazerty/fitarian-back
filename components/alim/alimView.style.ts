import css from 'styled-jsx/css'

export default css.global`

h1 {
  margin-left: 10%;
}

.compos{
  height: 450px;
  display: block;
  width: 100%;
  overflow: auto;
}

.text-center {
  text-align: center;
}

.text-left {
  text-align: left;
}

.info-text {
  margin: 20px;
}

.compo-list tr:nth-child(odd) {
  background-color: #DDD7DC;
 }

 .help-button {
  width: 20px;
  height: 20px;
  background: linear-gradient(to bottom, #4eb5e5 0%,#389ed5 100%); /* W3C */
  border: none;
  border-radius: 10px;
  position: relative;
  border-bottom: 4px solid #2b8bc6;
  color: #fbfbfb;
  font-weight: 600;
  font-size: 13px;
  cursor: pointer;
}

 .modal-info__content{
   display:flex;
   flex-direction: column;
 }

 .modal-info__text{
   font-size: 15px;
   max-width: 400px;
   font-weight: 300;
   margin-top: 15px;
 }

 .modal-info__close-button {
  align-self:flex-end;
  right: 0;
  top: 0;
  padding: 8px 16px;
  display: inline-block;
  border-radius: 2px;
  vertical-align: middle;
  color: white;
  font-weight: bold;
  font-size: 18px;
  background-color: rgb(22, 49, 202);
  text-align: center;
  }


.groups{
    color: blue;
    margin-bottom: 15px;
}

.compo-list {
  font-family: "Times New Roman", Times, serif;
  border: 1px solid #FFFFFF;
  width: 600px;
  height: 200px;
  text-align: center;
  border-collapse: collapse;
}
.compo-list td, .compo-list th {
  border: 1px solid #FFFFFF;
  padding: 3px 2px;
}
.compo-list tbody td {
  font-size: 13px;
}
.compo-list tr:nth-child(even) {
  background: #D0E4F5;
}
.compo-list thead {
  background: #0B6FA4;
  border-bottom: 5px solid #FFFFFF;
}
.compo-list thead th {
  font-size: 17px;
  font-weight: bold;
  color: blue;
  text-align: center;
  border-left: 2px solid #FFFFFF;
}
.compo-list thead th:first-child {
  border-left: none;
}

.compo-list tfoot {
  font-size: 15px;
  font-weight: bold;
  color: #333333;
  background: #D0E4F5;
  border-top: 3px solid #444444;
}
.compo-list tfoot td {
  font-size: 15px;
}

`