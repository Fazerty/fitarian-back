import { Alim, Compo } from '../../server/entities';
import React from 'react'; // For jest
import { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from '@apollo/react-components';
import alimStyle from './alimView.style';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { SystemState } from '../../store/system/types';
import Modal from 'react-modal';

interface AlimProps {
  system: SystemState;
  alimId: number | undefined;
}

export class AlimSearch extends Component<AlimProps> {
  state: { modalIsOpen: boolean; info: string } = {
    modalIsOpen: false,
    info: '',
  };

  render() {
    const { modalIsOpen } = this.state;

    /**
     * Opens the info modal. Used to display info when a help button is clicked
     *
     */
    const openModal = (info: string) => {
      this.setState({ info, modalIsOpen: true });
    };

    /**
     * Closes the info modal.
     *
     */
    const closeModal = () => {
      this.setState({ info: '', modalIsOpen: false });
    };

    const infoModalStyle = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        minHeight: '80px',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
      },
    };
    /**
     * Query to retreive alims based on a search string.
     * Fields for name are included depending on the language.
     *
     * Rem:
     * The english argument should be removed.
     * alim_nom_eng and alim_nom_fr should be replaced by a field alim_nom (the name in the selected language).
     *
     */

    const QUERY = gql`
      query GetAlim($id: Int!, $english: Boolean!) {
        alim(id: $id) {
          id
          alim_code
          alim_nom_eng @include(if: $english)
          alim_nom_fr @skip(if: $english)
          alim_grp {
            alim_grp_nom_fr @skip(if: $english)
            alim_grp_nom_eng @include(if: $english)
            alim_ssgrp_nom_fr @skip(if: $english)
            alim_ssgrp_nom_eng @include(if: $english)
            alim_ssgrp_nom_fr @skip(if: $english)
            alim_ssgrp_nom_eng @include(if: $english)
          }
          compos {
            const {
              const_nom_fr @skip(if: $english)
              const_nom_eng @include(if: $english)
            }
            source {
              ref_citation
            }
            teneur
            min
            max
            code_confiance
          }
        }
      }
    `;
    return (
      <Query<any, any>
        query={QUERY}
        variables={{
          id: this.props.alimId,
          english: this.props.system.language === 'en',
        }}
        onError={(_error: any) => {}}
      >
        {({ data }: { data: any }) => {
          if (data) {
            const { alim }: { alim: Partial<Alim> } = data;
            return (
              <div>
                <style jsx={undefined}>{alimStyle}</style>
                <h1>
                  {alim.alim_nom_eng ? alim.alim_nom_eng : alim.alim_nom_fr}
                </h1>
                <div className="groups">
                  {alim.alim_grp
                    ? (alim.alim_grp.alim_grp_nom_fr
                        ? alim.alim_grp.alim_grp_nom_fr
                        : alim.alim_grp.alim_grp_nom_eng)!
                    : ''}

                  {alim.alim_grp &&
                  (alim.alim_grp.alim_ssgrp_nom_fr
                    ? alim.alim_grp.alim_ssgrp_nom_fr
                    : alim.alim_grp.alim_ssgrp_nom_eng)
                    ? ' >> ' +
                      (alim.alim_grp.alim_ssgrp_nom_fr
                        ? alim.alim_grp.alim_ssgrp_nom_fr
                        : alim.alim_grp.alim_ssgrp_nom_eng)!
                    : ''}
                  {alim.alim_grp &&
                  (alim.alim_grp.alim_ssssgrp_nom_fr
                    ? alim.alim_grp.alim_ssssgrp_nom_fr
                    : alim.alim_grp.alim_ssssgrp_nom_eng)
                    ? ' >> ' +
                      (alim.alim_grp.alim_ssssgrp_nom_fr
                        ? alim.alim_grp.alim_ssssgrp_nom_fr
                        : alim.alim_grp.alim_ssssgrp_nom_eng)!
                    : ''}
                </div>

                {/* Results */}
                <div className="compos">
                  <table className="compo-list">
                    <thead>
                      <tr>
                        <th>
                          {this.props.system.language === 'en'
                            ? 'Component'
                            : 'Constituant'}
                        </th>
                        <th>
                          {this.props.system.language === 'en'
                            ? 'Content'
                            : 'Teneur'}
                        </th>
                        <th>
                          {this.props.system.language === 'en'
                            ? 'min/max'
                            : 'min/max'}
                        </th>
                        <th>
                          <span>
                            {this.props.system.language === 'en'
                              ? 'Confidence'
                              : 'Confiance'}
                          </span>
                          &nbsp;
                          <button
                            onClick={e =>
                              openModal((e.target as HTMLElement).title)
                            }
                            className="help-button"
                            title={
                              this.props.system.language === 'en'
                                ? 'Characterizes the quality of the average content value(A = very reliable to D = less reliable)'
                                : 'Caractérise la qualité de la valeur moyenne du contenu (A = très fiable à D = moins fiable)'
                            }
                          >
                            ?
                          </button>
                        </th>
                        <th>
                          {this.props.system.language === 'en'
                            ? 'Source'
                            : 'Source'}
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {alim.compos ? (
                        alim.compos.map((compo: Partial<Compo>) => (
                          <tr key={compo.id} className="text-center">
                            <td className="text-left">
                              {compo.const
                                ? compo.const.const_nom_eng ||
                                  compo.const.const_nom_fr
                                : ''}
                            </td>
                            <td>{compo.teneur}</td>
                            <td>
                              {compo.min}/{compo.max}
                            </td>
                            <td>{compo.code_confiance}</td>
                            <td>
                              {compo.source ? (
                                <button
                                  className="help-button"
                                  onClick={e =>
                                    openModal((e.target as HTMLElement).title)
                                  }
                                  title={compo.source.ref_citation}
                                >
                                  ?
                                </button>
                              ) : (
                                ''
                              )}
                            </td>
                          </tr>
                        ))
                      ) : (
                        <p></p>
                      )}
                    </tbody>
                  </table>
                </div>
                <Modal
                  style={infoModalStyle}
                  isOpen={modalIsOpen}
                  onRequestClose={closeModal}
                  contentLabel="modal-info"
                >
                  <div className="modal-info__content">
                    <div
                      onClick={closeModal}
                      className="modal-info__close-button"
                    >
                      &times;
                    </div>
                    <div className="modal-info__text">{this.state.info}</div>
                  </div>
                </Modal>
              </div>
            );
          }
          return (
            <div>
              <style jsx={undefined}>{alimStyle}</style>
              ??
            </div>
          );
        }}
      </Query>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const AlimSearchRedux = connect(mapStateToProps, {})(AlimSearch);

export default AlimSearchRedux;
