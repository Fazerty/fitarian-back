import React from 'react';
import { Alim } from '../../server/entities';
import { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from '@apollo/react-components';
import alimSearchStyle from './alimSearch.style';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { SystemState } from '../../store/system/types';
import { AlimResult } from '../../server/resolvers/types/alimResult';
import Modal from 'react-modal';
import AlimView from './alimView.component';

interface AlimSearchProps {
  system: SystemState;
}

const customModalStyle = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

const isOdd = (num: number) => {
  return num % 2 == 1;
};
export class AlimSearch extends Component<AlimSearchProps> {
  state: {
    search: string;
    limit: number;
    offset: number;
    selectedAlimId: number | undefined;
    modalIsOpen: boolean;
  } = {
    search: '',
    limit: 10,
    offset: 0,
    selectedAlimId: undefined,
    modalIsOpen: false,
  };

  render() {
    const { search, offset, modalIsOpen } = this.state;

    const openModal = (id: number | undefined) => {
      this.setState({ selectedAlimId: id, modalIsOpen: true });
    };

    const closeModal = () => {
      this.setState({ selectedAlimId: undefined, modalIsOpen: false });
    };

    /**
     * Query to retreive alims based on a search string.
     * Fields for name are included depending on the language.
     *
     * Rem:
     * The english argument should be removed.
     * alim_nom_eng and alim_nom_fr should be replaced by a field alim_nom (the name in the selected language).
     *
     */
    const QUERY = gql`
      query GetAlims(
        $search: String!
        $lang: String!
        $english: Boolean!
        $offset: Int!
      ) {
        alims(search: $search, lang: $lang, offset: $offset) {
          result {
            id
            alim_code
            alim_nom_eng @include(if: $english)
            alim_nom_fr @skip(if: $english)
          }
          count
        }
      }
    `;

    return (
      <Query<any, any>
        query={QUERY}
        variables={{
          search,
          lang: this.props.system.language,
          english: this.props.system.language === 'en',
          offset,
        }}
        onError={(_error: any) => {}}
      >
        {({ data }: { data: any }) => {
          if (data) {
            const { alims }: { alims: AlimResult } = data;
            return (
              <div className="alim-search">
                <style jsx={undefined}>{alimSearchStyle}</style>

                <div className="alim-search__title">Fitarian</div>

                <div className="alim-search__subtitle">
                  {this.props.system.language === 'en'
                    ? 'A food composition table'
                    : 'Une table de composition des aliments'}
                </div>

                {/* Search field */}
                <input
                  className="alim-search__input"
                  type="text"
                  value={this.state.search}
                  onChange={e =>
                    this.setState({ search: e.target.value, offset: 0 })
                  }
                  placeholder={
                    this.props.system.language === 'en' ? 'Search' : 'Recherche'
                  }
                ></input>

                {/* Results */}
                <div className="alim-search__results">
                  <div className="alim-search__result-list">
                    {alims.result.length === 0 && (
                      <div className="alim-search__result">
                        {this.props.system.language === 'en'
                          ? 'No result'
                          : 'Pas de résultat'}
                      </div>
                    )}
                    {alims.result.length !== 0 && (
                      <div className="alim-search__result-header">
                        {this.props.system.language === 'en' ? 'Name' : 'Nom'}
                      </div>
                    )}
                    {alims.result.length !== 0 &&
                      alims.result.map((alim: Partial<Alim>, index: number) => {
                        if (!isOdd(index))
                          return (
                            <div key={alim.id} className="alim-search__result">
                              <a onClick={() => openModal(alim.id)}>
                                {alim.alim_nom_eng || alim.alim_nom_fr}
                              </a>
                            </div>
                          );
                        else
                          return (
                            <div
                              key={alim.id}
                              className="alim-search__result alim-search__result--light"
                            >
                              <a onClick={() => openModal(alim.id)}>
                                {alim.alim_nom_eng || alim.alim_nom_fr}
                              </a>
                            </div>
                          );
                      })}
                  </div>
                </div>

                {/* Pagination */}
                <div className="pagination">
                  <button
                    className="pagination__button pagination__button--rounded-left"
                    disabled={offset === 0}
                    onClick={() => this.setState({ offset: 0 })}
                  >
                    &lt;&lt;
                  </button>
                  <button
                    className="pagination__button"
                    disabled={offset === 0}
                    onClick={() =>
                      this.setState({ offset: offset - 1 * this.state.limit })
                    }
                  >
                    &lt;
                  </button>
                  {/* Page information */}
                  <div className="results-info">
                    <span className="results-info__page-number">
                      {Math.floor(offset / this.state.limit) + 1}
                    </span>
                    <span className="results-info__total-pages">
                      {' '}
                      /{' ' + (Math.floor(alims.count / this.state.limit) + 1)}
                    </span>
                  </div>
                  <button
                    className="pagination__button"
                    disabled={
                      Math.floor(offset / this.state.limit) + 1 >=
                      Math.floor(alims.count / this.state.limit) + 1
                    }
                    onClick={() =>
                      this.setState({ offset: offset + 1 * this.state.limit })
                    }
                  >
                    &gt;
                  </button>
                  <button
                    className="pagination__button pagination__button--rounded-right"
                    disabled={
                      Math.floor(offset / this.state.limit) + 1 >=
                      Math.floor(alims.count / this.state.limit) + 1
                    }
                    onClick={() =>
                      this.setState({
                        offset:
                          Math.floor(alims.count / this.state.limit) *
                          this.state.limit,
                      })
                    }
                  >
                    &gt;&gt;
                  </button>
                </div>

                {/* Modal to display a selected alim */}
                <Modal
                  isOpen={modalIsOpen}
                  onRequestClose={closeModal}
                  contentLabel="Alim view"
                  style={customModalStyle}
                >
                  <div className="modal">
                    <div onClick={closeModal} className="modal__close-button">
                      &times;
                    </div>
                    {this.state.selectedAlimId ? (
                      <AlimView alimId={this.state.selectedAlimId} />
                    ) : (
                      ''
                    )}
                  </div>
                </Modal>
              </div>
            );
          }
          return (
            <div className="alim-search">
              <style jsx={undefined}>{alimSearchStyle}</style>

              <div className="alim-search__title">Fitarian</div>

              <div className="alim-search__subtitle">
                {this.props.system.language === 'en'
                  ? 'A food composition table'
                  : 'Une table de composition des aliments'}
              </div>
              <input
                className="alim-search__input"
                type="text"
                value={this.state.search}
                onChange={e => this.setState({ search: e.target.value })}
              ></input>
            </div>
          );
        }}
      </Query>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const AlimSearchRedux = connect(mapStateToProps, {})(AlimSearch);

export default AlimSearchRedux;
