import React, { Component } from 'react';
import contactStyle from './contact.style';
import { SystemState } from '../store/system/types';
import { connect } from 'react-redux';
import { AppState } from '../store/store';

interface ContactProps {
  system: SystemState;
}

export class Contact extends Component<ContactProps> {
  render() {
    return (
      <div className="contact">
        <style jsx={undefined}>{contactStyle}</style>
        <div className="contact__content">

          <div className="contact__title">Portfolio</div>

          {this.props.system.language === 'en' ? (
            <div>
              <div className="contact__text">
                This project was made for my portfolio:{' '}
                <a href="https://portfolio.upurion.com">
                  https://portfolio.upurion.com
                </a>
              </div>
              <div className="contact__text">
                The source code is available at{' '}
                <a href="https://bitbucket.org/Fazerty/fitarian-back">
                  bitbucket
                </a>
              </div>
            </div>
          ) : (
            <div>
              <div className="contact__text">
                Ce project a été conçu pour mon portfolio:{' '}
                <a href="https://portfolio.upurion.com">
                  https://portfolio.upurion.com
                </a>
              </div>
              <div className="contact__text">
                Le code source is disponible sur{' '}
                <a href="https://bitbucket.org/Fazerty/fitarian-back">
                  bitbucket
                </a>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const ContactRedux = connect(mapStateToProps, {})(Contact);

export default ContactRedux;
