import css from 'styled-jsx/css'

export default css`

.contact{
  display:flex;
  flex-direction: column;
  width: 600px;
  padding: 20px;
}


.contact__content{
  padding: 20px;
  background: white;
}

.contact__title{
  font-weight: bold;
  font-size: 30px;
  margin: 15px;
}

.contact__text{
  color: gray;
  margin-bottom: 5px;
}
`