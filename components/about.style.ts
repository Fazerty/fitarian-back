import css from 'styled-jsx/css'

export default css.global`

.about{
  display:flex;
  flex-direction: column;
  width: 800px;
  padding: 20px;
}

.about__content{
  padding: 20px;
  background: white;
}

.about__title{
  font-weight: bold;
  font-size: 30px;
  margin: 15px;
}

.about__title--small{
  font-size: 25px;
}

.rows {
  display: flex;
  flex-direction: row;
  align-items:center;
}
.rows > div {
  padding: 5px;
}

.about__text{
  color: gray;
  margin-bottom: 5px;
}


`