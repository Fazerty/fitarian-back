import css from 'styled-jsx/css'

export default css.global`

.news{
  display:flex;
  flex-direction: column;
  width: 800px;
  padding: 20px;
}

.news__content{
  padding: 20px;
  background: white;
}

.news__title{
  font-weight: bold;
  font-size: 30px;
  margin: 15px;
}

.news__title--small{
  font-size: 25px;
}

.rows {
  display: flex;
  flex-direction: row;
  align-items:center;
}
.rows > div {
  padding: 5px;
}

.news__text{
  color: gray;
  margin-bottom: 5px;
}


`