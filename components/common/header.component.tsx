import React from 'react'; // For jest
import { Component } from 'react';
import headerStyle from './header.style';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { AppState } from '../../store/store';
import { updateLanguage } from '../../store/system/actions';
import { SystemState } from '../../store/system/types';
// import { MessageState } from '../../store/message/types';

interface HeaderProps {
  updateLanguage: typeof updateLanguage;
  system: SystemState;
  // message: MessageState;
}

export class Header extends Component<HeaderProps> {
  state = {
    search: '',
  };

  render() {
    return (
      <>
        <style jsx={undefined}>{headerStyle}</style>
        <div className="navbar">
          <div className="navbar__links">
            <NavLink
              className="navbar__link"
              activeClassName="navbar__link--active"
              to=""
              exact
            >
              Home
            </NavLink>
            <NavLink
              className="navbar__link"
              activeClassName="navbar__link--active"
              to="news"
            >
              News
            </NavLink>
            <NavLink
              className="navbar__link"
              activeClassName="navbar__link--active"
              to="contact"
            >
              Contact
            </NavLink>
            <NavLink
              className="navbar__link"
              activeClassName="navbar__link--active"
              to="about"
            >
              About
            </NavLink>
            <a
              className="navbar__link"
              onClick={() =>
                this.props.updateLanguage(
                  this.props.system.language === 'en' ? 'fr' : 'en',
                )
              }
            >
              {this.props.system.language === 'en' ? 'EN/fr' : 'en/FR'}
            </a>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const HeaderRedux = connect(mapStateToProps, { updateLanguage })(Header);

export default HeaderRedux;
