import {} from '@apollo/react-hooks';
import { Component } from 'react';
import footerStyle from './footer.style';
import React from 'react'; // For jest

export default class Footer extends Component {
  state = {
    search: '',
    lang: 'en',
  };

  render() {
    return (
      <footer className="footer">
        <style jsx={undefined}>{footerStyle}</style>
        <div className="footer__content">
          <p className="footer__title">Fitarian Server</p>
          <p className="footer__info footer__info--grey">footer content.</p>
        </div>
      </footer>
    );
  }
}
