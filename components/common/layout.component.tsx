import Header from './header.component';
import Footer from './footer.component';
import React, { Component } from 'react';
import layoutStyle from './layout.style';
import ReactModal from 'react-modal';

if (typeof window !== 'undefined') {
  ReactModal.setAppElement('body');
}

export class Layout extends Component {
  render() {
    return (
      <div className="layout">
        <style jsx={undefined}>{layoutStyle}</style>
        <Header />
        <div className="layout__main-section">
          <div className="layout__content">{this.props.children}</div>
        </div>
        <Footer />
      </div>
    );
  }
}
