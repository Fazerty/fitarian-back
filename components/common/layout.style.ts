import css from 'styled-jsx/css'

export default css`


.layout__content {
  background-color: rgba(100, 95, 95, 0.541);
  width: 100%;
  height: 70%;
  margin-top: 2%;
  display: flex;
  flex-direction: column;
  align-items: center;
}
.layout__main-section{
  display: flex;
  align-self: strech;
  flex: 1;
}

.layout {
  // Photo by Dan Gold on Unsplash
 background-image: url("dan-gold-4_jhDO54BYg-unsplash.jpg");
 background-position: center; /* Center the image */
 background-repeat: no-repeat; /* Do not repeat the image */
 background-size: cover; /* Resize the background image to cover the entire container */
 position:absolute;
 width: 100%;
 height: 100%;
 display: flex;
 flex-direction: column;
 justify-content: space-around;
}
`