import css from 'styled-jsx/css'

export default css`
.footer {
  width: 100%;
  bottom: 0;
  position: fixed;
  display: flex ;
  justify-content: flex-end;
}


.footer__content {
  background-color: rgb(75, 73, 73);
  padding-left: 3em;
  padding-right: 2em;
  border-top-left-radius: 5em;
  border-bottom-left-radius: 2em;
  display: flex ;
  justify-content: stretch;
  align-items: baseline
}

.footer__title{
  font-size: 1.2em;
  padding: 0.5em;
}
.footer__info--grey{
  color: grey;
}

`