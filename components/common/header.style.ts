import css from 'styled-jsx/css'

export default css.global`

.navbar {
  overflow: hidden;
  display: flex;
  justify-content: flex-end;
}

.navbar__links {
  display: flex;
  padding-left: 3em;
  background-color: #333;
  border-top-left-radius: 2em;
  border-bottom-left-radius: 8em;
}

.navbar__link {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  margin: 2px;
  text-decoration: none;
}

/* Change the link color to #111 (black) on hover */
.navbar__link:hover {
  background-color: #111;
  border-radius: 2em;
}

.navbar__link--active {
  border-radius: 2em;
  background-color: #4CAF50;
}

`