import React, { Component } from 'react';
import newsStyle from './news.style';
import { SystemState } from '../store/system/types';
import { connect } from 'react-redux';
import { AppState } from '../store/store';

interface NewsProps {
  system: SystemState;
}

export class News extends Component<NewsProps> {
  render() {
    return (
      <div className="news">
        <style jsx={undefined}>{newsStyle}</style>
        <div className="news__content">
          <div className="news__title">
            {this.props.system.language === 'en' ? 'News' : 'Nouvelles'}
          </div>
          <div className="news__text">
            {this.props.system.language === 'en' ? 'no news' : 'pas de nouvelles'}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const NewsRedux = connect(mapStateToProps, {})(News);

export default NewsRedux;
