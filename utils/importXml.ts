import * as fs from 'fs';
import { OptionsV2, parseStringPromise } from 'xml2js';
import * as path from 'path';
import {decode } from 'iconv-lite';

export class ImportXml {

  static parserOptions: OptionsV2 = {
    trim: true,
    explicitArray: false,
  };

  static async importXml(xmlPath: string): Promise<any> {
    const data: string = decode(fs.readFileSync(path.join(__dirname, xmlPath)), 'windows-1252');
    const result = await parseStringPromise(data, ImportXml.parserOptions);
    return result;
  }

}