import { ImportXml } from "./importXml";
import { getCustomRepository } from "typeorm";

import { AlimRepository } from "../server/repository/alimRepository";
import { AlimGrpRepository } from "../server/repository/alimGrpRepository";
import { CompoRepository } from "../server/repository/compoRepository";
import { ConstRepository } from "../server/repository/constRepository";
import { SourceRepository } from "../server/repository/sourceRepository";
import {
  // Alim,
  AlimGrp, Compo, Const, Source, Alim
} from "../server/entities";
import { logger } from "./logger";
import {
  AlimGrpService,
  // AlimService,
  CompoService, ConstService, SourceService, AlimService
} from "../server/service";

export class ImportXmlService {



  /**
   * Reads data form the alim, alimGrp, compo, const and soource xml files
   * and writes values in the alim, alimGrp, compo, const and source tables.
   *
   * @static
   * @memberof ImportXmlService
   */

  static async importAll() {

    // If true, the tables will be emptied.
    const clear: boolean = false;

    logger.info('importing xml');

    const alimRepository: AlimRepository = getCustomRepository(AlimRepository);
    const alimService: AlimService = new AlimService();
    const alimGrpRepository: AlimGrpRepository = getCustomRepository(AlimGrpRepository);
    const alimGrpService: AlimGrpService = new AlimGrpService();
    const compoRepository: CompoRepository = getCustomRepository(CompoRepository);
    const compoService: CompoService = new CompoService();
    const constRepository: ConstRepository = getCustomRepository(ConstRepository);
    const constService: ConstService = new ConstService();
    const sourceRepository: SourceRepository = getCustomRepository(SourceRepository);
    const sourceService: SourceService = new SourceService();

    // Grp code -> grp id. It will be used to create links between entities
    const grpCodeId: Map<number, number> = new Map<number, number>();
    // alim code -> alim id. It will be used to create links between entities
    const alimCodeId: Map<number, number> = new Map<number, number>();
    // source code -> source id. It will be used to create links between entities
    const sourceCodeId: Map<number, number> = new Map<number, number>();
    // const code -> const id. It will be used to create links between entities
    const cnstCodeId: Map<number, number> = new Map<number, number>();

    if (clear) {
      await alimRepository.query('TRUNCATE "alim" CASCADE');
      await alimGrpRepository.query('TRUNCATE "alim_grp" CASCADE');
      await compoRepository.query('TRUNCATE "compo" CASCADE');
      await constRepository.query('TRUNCATE "const" CASCADE');
      await sourceRepository.query('TRUNCATE "source" CASCADE');
      logger.info('tables cleared');
    }

    if (await alimGrpRepository.count() === 0) {
      const alimGrpResult: any = await ImportXml.importXml('../xml/../xml/alim_grp_2017 11 21.xml');
      const length: number = alimGrpResult.TABLE.ALIM_GRP.length;
      let count: number = 0;
      for (const element of alimGrpResult.TABLE.ALIM_GRP as {
        alim_grp_code: string,
        alim_grp_nom_fr: string,
        alim_grp_nom_eng: string,
        alim_ssgrp_code: string,
        alim_ssgrp_nom_fr: string,
        alim_ssgrp_nom_eng: string,
        alim_ssssgrp_code: string,
        alim_ssssgrp_nom_fr: string,
        alim_ssssgrp_nom_eng: string
      }[]) {
        const grp: AlimGrp | undefined = await alimGrpService.create(element);
        process.stdout.write("Processing alimGrp: " + ++count + " /" + length + "\r");
        if (grp) {
          grpCodeId.set(grp.grp_code as number, grp.id as number);
        }
      };

      logger.info(length + ' alim grps imported');
      // rem : A lot of redundancy in the xml and missing groups and subgroups (401, 403, 503, 8 are needed in alim).
      // Table were created from the xml structure. It could be changed later to avoid redundancy.
      await alimGrpService.createMissingGrps();
    }
      // Import data only if tables are empty.
      if (await alimRepository.count() === 0) {
        const alimResult: any = await ImportXml.importXml('../xml/alim_2017 11 21.xml');
        const length: number = alimResult.TABLE.ALIM.length;
        let count: number = 0;
        for (const element of alimResult.TABLE.ALIM as {
          alim_code: string,
          alim_nom_fr: string,
          alim_nom_index_fr: string,
          alim_nom_eng: string,
          alim_nom_index_eng: string,
          alim_grp_code: string,
          alim_ssgrp_code: string,
          alim_ssssgrp_code: string
        }[]) {
          const alim: Alim | undefined = await alimService.create(element, grpCodeId);
          process.stdout.write("Processing alim: " + ++count + " /"  + length + "\r");
          if (alim) {
            alimCodeId.set(alim.alim_code as number, alim.id as number);
          }
        };
        logger.info(length + ' alims imported');
      }

      if (await sourceRepository.count() === 0) {
        const sourceResult: any = await ImportXml.importXml('../xml/../xml/sources_2017 11 21.xml');
        const length: number = sourceResult.TABLE.SOURCES.length;
        let count: number = 0;
        for (const element of sourceResult.TABLE.SOURCES as {
          source_code: string,
          ref_citation: string
        }[]) {
          const source: Source | undefined = await sourceService.create(element);
          process.stdout.write("Processing source: " + ++count + " /"  + length + "\r");
          if (source) {
            sourceCodeId.set(source.source_code as number, source.id as number);
          }
        };
        logger.info(length + ' sources imported');
      }


      if (await constRepository.count() === 0) {
        const constResult: any = await ImportXml.importXml('../xml/../xml/const_2017 11 21.xml');
        const length: number = constResult.TABLE.CONST.length;
        let count: number = 0;
        for (const element of constResult.TABLE.CONST as {
          const_code: string,
          const_nom_fr: string,
          const_nom_eng: string
        }[]) {
          const cnst: Const | undefined = await constService.create(element);
          process.stdout.write("Processing const: " + ++count + " /"  + length + "\r");
          if (cnst) {
            cnstCodeId.set(cnst.const_code as number, cnst.id as number);
          }
        };
        logger.info(length + ' consts imported');
      }

      if (await compoRepository.count() === 0) {
        const compoResult: any = await ImportXml.importXml('../xml/../xml/compo_2017 11 21.xml');
        const length: number = compoResult.TABLE.COMPO.length;
        let count: number = 0;
        for (const element of compoResult.TABLE.COMPO as {
          alim_code: string,
          const_code: string,
          teneur: string,
          min: string | { '$': { missing: string } },
          max: string | { '$': { missing: string } },
          code_confiance: string | { '$': { missing: string } },
          source_code: string | { '$': { missing: string } }
        }[]) {
          const compo: Compo | undefined = await compoService.create(element, alimCodeId, cnstCodeId, sourceCodeId);
          process.stdout.write("Processing compo: " + ++count + " /"  + length + "\r");
          if (compo) {
          }
        };
        logger.info(length + ' compos imported');
      }

    }

}