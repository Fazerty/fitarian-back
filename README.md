# TypeScript and GraphQL Portofolio Project

This project was made to learn graphql and react.

Test it at https://fitarian.upurion.com

## How to use

Download

```bash
git clone https://bitbucket.org/Fazerty/fitarian-back.git
```

Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

