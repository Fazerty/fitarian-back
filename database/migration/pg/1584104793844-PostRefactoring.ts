import {MigrationInterface, QueryRunner} from "typeorm";

export class PostRefactoring1584104793844 implements MigrationInterface {
    name = 'PostRefactoring1584104793844'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "alim" ("id" SERIAL NOT NULL, "alim_code" integer NOT NULL, "alim_nom_fr" character varying NOT NULL, "alim_nom_index_fr" character varying NOT NULL, "alim_nom_eng" character varying NOT NULL, "alim_nom_index_eng" character varying NOT NULL, "alim_grp_code" integer NOT NULL, "alim_ssgrp_code" integer NOT NULL, "alim_ssssgrp_code" integer NOT NULL, CONSTRAINT "PK_a60594bc49ef3092b680dd653d5" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "alim_grp" ("id" SERIAL NOT NULL, "alim_grp_code" integer NOT NULL, "alim_grp_nom_fr" character varying NOT NULL, "alim_grp_nom_eng" character varying NOT NULL, "alim_ssgrp_code" integer NOT NULL, "alim_ssgrp_nom_fr" character varying NOT NULL, "alim_ssgrp_nom_eng" character varying NOT NULL, "alim_ssssgrp_code" integer NOT NULL, "alim_ssssgrp_nom_fr" character varying NOT NULL, "alim_ssssgrp_nom_eng" character varying NOT NULL, CONSTRAINT "PK_62c7e24ccbc0bc75596e71267a9" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "compo" ("id" SERIAL NOT NULL, "alim_code" integer NOT NULL, "const_code" integer NOT NULL, "teneur" character varying NOT NULL, CONSTRAINT "PK_8bcfb71a377fdd14f280e3bd75e" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "const" ("id" SERIAL NOT NULL, "const_code" integer NOT NULL, "const_nom_fr" character varying NOT NULL, "const_nom_eng" character varying NOT NULL, CONSTRAINT "PK_5e1800e170766ef3fc3bfacf4cc" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "source" ("id" SERIAL NOT NULL, "source_code" integer NOT NULL, "ref_citation" character varying NOT NULL, CONSTRAINT "PK_018c433f8264b58c86363eaadde" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "source"`, undefined);
        await queryRunner.query(`DROP TABLE "const"`, undefined);
        await queryRunner.query(`DROP TABLE "compo"`, undefined);
        await queryRunner.query(`DROP TABLE "alim_grp"`, undefined);
        await queryRunner.query(`DROP TABLE "alim"`, undefined);
    }

}
