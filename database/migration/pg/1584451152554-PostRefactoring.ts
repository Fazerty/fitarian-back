import {MigrationInterface, QueryRunner} from "typeorm";

export class PostRefactoring1584451152554 implements MigrationInterface {
    name = 'PostRefactoring1584451152554'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "alim_grp" ADD "grp_code" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "min" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "max" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "code_confiance" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "source_code" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "alimId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "constId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD "sourceId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ADD "alimGrpId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD CONSTRAINT "FK_947b7704b3579fd4f421781613c" FOREIGN KEY ("alimId") REFERENCES "alim"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD CONSTRAINT "FK_2dce0ee6be6eae10bf8a8e8b12a" FOREIGN KEY ("constId") REFERENCES "const"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ADD CONSTRAINT "FK_42f04342cb61187387f06aebc92" FOREIGN KEY ("sourceId") REFERENCES "source"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ADD CONSTRAINT "FK_71d0edbbd887c9296a097084954" FOREIGN KEY ("alimGrpId") REFERENCES "alim_grp"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "alim" DROP CONSTRAINT "FK_71d0edbbd887c9296a097084954"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP CONSTRAINT "FK_42f04342cb61187387f06aebc92"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP CONSTRAINT "FK_2dce0ee6be6eae10bf8a8e8b12a"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP CONSTRAINT "FK_947b7704b3579fd4f421781613c"`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" DROP COLUMN "alimGrpId"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "sourceId"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "constId"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "alimId"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "source_code"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "code_confiance"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "max"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" DROP COLUMN "min"`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" DROP COLUMN "grp_code"`, undefined);
        await queryRunner.query(`DROP TABLE "source"`, undefined);
    }

}
