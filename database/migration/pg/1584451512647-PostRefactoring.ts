import {MigrationInterface, QueryRunner} from "typeorm";

export class PostRefactoring1584451512647 implements MigrationInterface {
    name = 'PostRefactoring1584451512647'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "grp_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_grp_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_grp_nom_fr" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_grp_nom_eng" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssgrp_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssgrp_nom_fr" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssgrp_nom_eng" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssssgrp_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssssgrp_nom_fr" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssssgrp_nom_eng" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "const" ALTER COLUMN "const_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "const" ALTER COLUMN "const_nom_fr" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "const" ALTER COLUMN "const_nom_eng" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "source" ALTER COLUMN "source_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "source" ALTER COLUMN "ref_citation" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "alim_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "const_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "teneur" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "min" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "max" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "code_confiance" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "source_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_fr" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_index_fr" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_eng" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_index_eng" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_grp_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_ssgrp_code" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_ssssgrp_code" DROP NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_ssssgrp_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_ssgrp_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_grp_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_index_eng" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_eng" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_index_fr" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_nom_fr" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" ALTER COLUMN "alim_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "source_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "code_confiance" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "max" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "min" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "teneur" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "const_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" ALTER COLUMN "alim_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "source" ALTER COLUMN "ref_citation" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "source" ALTER COLUMN "source_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "const" ALTER COLUMN "const_nom_eng" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "const" ALTER COLUMN "const_nom_fr" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "const" ALTER COLUMN "const_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssssgrp_nom_eng" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssssgrp_nom_fr" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssssgrp_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssgrp_nom_eng" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssgrp_nom_fr" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_ssgrp_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_grp_nom_eng" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_grp_nom_fr" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "alim_grp_code" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "alim_grp" ALTER COLUMN "grp_code" SET NOT NULL`, undefined);
    }

}
